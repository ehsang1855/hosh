#!/usr/bin/python3 
import random
import os

def sub_list(l1,l2):
    l1=list(reversed(l1))
    for i in l2:
        l1.remove(i)
    return list(reversed(l1))
# 
def mutate(individual,rate):
    for i in range(rate):
        i=random.randrange(len(individual))
        j=random.randrange(len(individual))
        individual[i],individual[j]=individual[j],individual[i]
#-----------------------------------------------------------------------------------
def crossover(father,mother,start_index,end_index,mutation_rate):
        
        sperm=father[start_index:end_index]
        fetus=sub_list(mother,sperm)

        baby=fetus[:start_index]+sperm+fetus[start_index:(len(fetus))]
        mutate(baby,mutation_rate)
        baby1=baby[:]
        return baby1
#-------------------------------------------------------------------------------------------
def evolve(population):
    new_population=Population()
    new_population.add(population.best_individual())
    for i in range(len(population.individuals)-1):
        mutation_rate=random.randrange(2)
        start_at=random.randrange(0,len(population.individuals[i].genome)-1)
        stop_at=random.randrange(start_at,len(population.individuals[i].genome)-1)
        baby=crossover(population.individuals[i].genome,population.individuals[i+1].genome,start_at,stop_at,mutation_rate)
        new_population.add(Individual(baby))
    del(population)
    return new_population
#-----------------------------------------------------------------------------------------------------
class Individual:
    def __init__(self,genome):
        self.genome=genome

    def fitness(self):
        fitness_check=[]
        j=-1
        for i in self.genome:
            if i>=j:
                fitness_check.append(i)
                j=i 
            else:
                break
        return (len(fitness_check))
#-------------------------------------------------------------------------------------------
class Population:
    
    def __init__(self):
        self.individuals=[]
#____________________________
    def add(self,individual):
        self.individuals.append(individual)
#---------------------------------------------------------------------------------------------
    def best_individual(self):
        best_fitness=0
        best_indv=None
        for individual in self.individuals:
            if individual.fitness() > best_fitness:
                best_fitness = individual.fitness()
                best_indv =individual
        return best_indv
#----------------------------------------------------------------------------------------------
def individual_gen(gene_array):
    for j in range(2,random.randrange(len(gene_array))):
        index1=random.randrange(len(gene_array))
        index2=random.randrange(len(gene_array))
        gene_array[index1],gene_array[index2]=gene_array[index2],gene_array[index1]
    gene_array1=gene_array[:]
    return gene_array1
#------------------------------------------------------------------------------------------------
def population_gen(population,gene_array,size):
    population.add(Individual(gene_array))
    for i in range(size):
        g=individual_gen(gene_array)
        population.add(Individual(g))
#---------------------------------------------------------------------------------------------------
def main():
    print("enter array by space beetwen each element for exampel: 1 2 3 6 \n")
    gene_array=list(map(int,input().split()))
    p=Population()
    population_gen(p,gene_array,10)
    generation=1
    while p.best_individual().fitness()<len(gene_array):
        os.system('clear')
        print("generation: "+str(generation)+"\n")
        p=evolve(p)
        print("fitness :"+str(p.best_individual().fitness())+"\n")
        print(p.best_individual().genome) 
        generation+=1
#    print(p.best_individual().genome) 
if __name__=="__main__":main()
